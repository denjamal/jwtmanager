﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using CommandLine;
using Microsoft.IdentityModel.Tokens;

namespace JwtManager
{
    class Options
    {
        [Option( longName:"auth0id", Required = true, HelpText = "Input Auth0 Id")]
        public string Auth0Id { get; set; }

        [Option(shortName:'i',"issuer", Default = "Sheffield", Required = false, HelpText = "Input issuer")]
        public string Issuer { get; set; }

        [Option(shortName: 'a', "audience", Default = "Sheffield", Required = false, HelpText = "Input audience")]
        public string Audience { get; set; }

        [Option(shortName: 's', "secret", Default = "secretKey123needmoreStrings", Required = false, HelpText = "Input secret")]
        public string Secret { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed(IssueToken);
        }

        private static void IssueToken(Options options)
        {
            var claim = new Claim(JwtRegisteredClaimNames.Sub, options.Auth0Id);

            var jwt = new JwtSecurityToken(
                issuer: options.Issuer,
                audience: options.Audience,
                claims: new List<Claim> { claim },
                expires: DateTime.UtcNow.AddYears(10),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(options.Secret)), SecurityAlgorithms.HmacSha256)
            );

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            Console.WriteLine($"Secret: {options.Secret}");
            Console.WriteLine($"Iss: {options.Issuer}");
            Console.WriteLine($"Aud: {options.Audience}");
            Console.WriteLine($"JWT: {encodedJwt}");
        }
    }
}
